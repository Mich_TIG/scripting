#!/bin/bash

###########################################################################################################
#Auteur: David Michallet	
														
#Date:06/2019

#Description : Récupérer les dalles Rasters (type Orthophotos) qui intersectents une zone vecteur.

#Dépendances : sed GDAL/OGR

#Préconisation: bien vérifier que l'ensemble des couches ont le meme systéme de coordonnées !

#############################################################################################################

creation_index_tuiles(){
  gdaltindex $choix_repertoire_travail/Grille_assemblage.shp $dossier_rasters/*.jp2
}

decouper_index_tuiles_avec_zone_etude(){
  ogr2ogr -a_srs EPSG:2154 -t_srs EPSG:2154 -s_srs EPSG:2154 -clipsrc $fichier_shp_zone_etude_shp_zone_etude $choix_repertoire_travail/Grille_assemblage_zone_etude.shp $choix_repertoire_travail/Grille_assemblage.shp
}

liste_dalles_intersects_zone_etude(){
  ogr2ogr -f CSV $choix_repertoire_travail/liste_fichier_shp_zone_etudes.csv $choix_repertoire_travail/Grille_assemblage_zone_etude.dbf -dialect sqlite -sql "SELECT replace(location,'.jp2','') as NOM_FICHIER FROM Grille_assemblage_zone_etude"
#suppression de la 1ere ligne
sed -i '1d' $choix_repertoire_travail/liste_fichier_shp_zone_etudes.csv
}

copier_dalles_vers_repertoire_travail(){
  for fichier_shp_zone_etude in $(<$choix_repertoire_travail/liste_fichier_shp_zone_etudes.csv);
    do
      cp "$fichier_shp_zone_etude".* $choix_repertoire_travail;
    done
}

main(){
  choix_repertoire_travail="/home/mich/scripts/Export_raster_BE/Travail"
  dossier_rasters="/home/mich/scripts/Export_raster_BE/dalles"
  fichier_shp_zone_etude_shp_zone_etude="zone_etude.shp"

  creation_index_tuiles
  decouper_index_tuiles_avec_zone_etude
  liste_dalles_intersects_zone_etude
  copier_dalles_vers_repertoire_travail

  # Afficher la liste des fichier_shp_zone_etudes copié
  cat /home/mich/scripts/Export_raster_BE/Travail/liste_fichier_shp_zone_etudes.csv
}
main
