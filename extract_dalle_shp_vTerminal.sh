#!/bin/bash

###########################################################################################################
#Auteur: David Michallet	
														
#Date:25/03/2019

#Description : Récupérer les dalles Rasters (type Orthophotos) qui intersectents une zone vecteur.

#Dépendances : zenity notify-send sed GDAL/OGR

#Explication :
#	Zenity = outil permettant d'insérer des boites de dialogue GTK+ permettant l'intéraction avec l'utilisateur.
#	notify-send = affiche une fenetre avec notification en haut à droite.
#	seb = manipuler des fichiers textes

#Préconisation: bien vérifier le systéme de coordonnées de vos couches sinon ça ne fonctionnera pas!

#############################################################################################################

#############################################################################################################
#############################################################################################################
#Pour une version sans boite de dialogue,renseigner les 3 variables en supprimant le hashtag # et commenter le paragraphe 1 en mettant des hashtag.

#Renseigner les variables en s'assurant que les dossiers existeint.
#REP_TRAVAIL=/home/$USER/MON_DOSSIER
#REP_RASTERS=
#FILE=
#
#if [ ! -d "$REP_TRAVAIL" ];then
#while true; do
#    read -p "Le répertoire $REP_TRAVAIL n'existe pas, voulez vous le créer (yn) ? " yn
#    case $yn in
#        [Yy]* ) mkdir $REP_TRAVAIL; break;;
#        [Nn]* ) echo "Merci de renseigner la variable \$REP_TRAVAIL"
#        exit;;
#        * ) echo "Merci de répondre par yes ou non."
#        exit;;
#    esac
#done
#fi
#if [ ! -d "$REP_RASTERS" ];then
#        echo "Merci de renseigner le répertoire (\$REP_RASTERS) contenant vos rasters !"
#        exit;
#fi
#
#
#if [ ! -f "$FILE" ];then
#echo "Le fichier n'existe pas, merci de renseigner le chemin d'acce !"
#        exit;
#fi
#
#
#############################################################################################################








#1/Sélection ou création d'un  répertoire où les dalles seront enregistrées.
REP_TRAVAIL=`zenity --file-selection --title="Sélectionnez un dossier où seront enregistrés vos rasters"  --text="Choisissez un dossier" --directory`

case $? in
        0)
        #      echo "\"$REP_TRAVAIL\" est sélectionné.";;
		notify-send --icon="info" "Vous avez sélectionné le repertoire \n \"$REP_TRAVAIL\" !" ":-)";;
        1)
              echo "Aucun fichier sélectionné.";;
        -1)
              echo "Une erreur inattendue est survenue.";;
esac

#2/Sélection du dossier contenant les Rasters et création de la grille d'assemblage des rasters.

REP_RASTERS=`zenity --file-selection --title="Sélectionnez le dossier contenants vos rasters"  --text="Choisissez un dossier" --directory`

case $? in
        0)
#              echo "\"$REP_RASTERS\" est sélectionné."
		notify-send --icon="info" "Vous avez selectionné l'emplacement $REP_RASTERS !" ":-)";;
        1)
              echo "Aucun fichier sélectionné.";;
        -1)
              echo "Une erreur inattendue est survenue.";;
esac

#3/Découper la grille d'assemblage en fonction du contour du site d'étude.

FILE=`zenity --file-selection --title="Sélectionnez le fichier à intersecter"  --text="Choisissez le fichier" `

case $? in
        0)
              echo "\"$FILE\" est sélectionné.";;
        1)
              echo "Aucun fichier sélectionné.";;
        -1)
              echo "Une erreur inattendue est survenue.";;
esac

#Execution des commandes

gdaltindex $REP_TRAVAIL/Grille_assemblage.shp $REP_RASTERS/*.jp2 && ogr2ogr -a_srs EPSG:2154 -t_srs EPSG:2154 -s_srs EPSG:2154 -clipsrc $FILE $REP_TRAVAIL/Grille_assemblage_zone_etude.shp $REP_TRAVAIL/Grille_assemblage.shp && ogr2ogr -f CSV $REP_TRAVAIL/liste_fichiers.csv $REP_TRAVAIL/Grille_assemblage_zone_etude.dbf -dialect sqlite -sql "SELECT replace(location,'.jp2','') as NOM_FICHIER FROM Grille_assemblage_zone_etude" && sed -i '1d' $REP_TRAVAIL/liste_fichiers.csv && for fichier in $(<$REP_TRAVAIL/liste_fichiers.csv); do cp "$fichier".* $REP_TRAVAIL; done 

#? utilisation de AWK pour recuperer les infos des noms de fichiers ?
#gdaltindex /home/mich/scripts/Export_rasters_BE/Travail/Grille_assemblage.shp $REP_RASTERS/*.jp3 | ogr2ogr -a_srs EPSG:2154 -t_srs EPSG:2154 -s_srs EPSG:2154 -clipsrc /home/mich/scripts/Export_rasters_BE/zone_etude.shp /home/mich/scripts/Export_rasters_BE/Travail/Grille_assemblage_zone_etude.shp $_
